plugins {
  kotlin("jvm") version "1.9.21"
  kotlin("kapt") version "1.9.21"
}

group = "org.njk.tech"
version = "1.0-SNAPSHOT"

repositories {
  mavenCentral()
}

val coroutinesVersion = "1.6.0"
val arrowVersion = "1.0.1"

dependencies {
  implementation(kotlin("stdlib-jdk8"))
  implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core", coroutinesVersion)
  implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-jdk8", coroutinesVersion)
  implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-debug", coroutinesVersion)
  implementation("org.apache.commons", "commons-lang3", "3.9")
  implementation("org.apache.poi:poi-ooxml:4.0.1")

  implementation("io.arrow-kt", "arrow-core", arrowVersion)
//  implementation("io.arrow-kt", "arrow-syntax", arrowVersion)
//  implementation("io.arrow-kt", "arrow-fx-coroutines", arrowVersion)
//  implementation("io.arrow-kt", "arrow-mtl", arrowVersion)
//  kapt("io.arrow-kt", "arrow-meta", arrowVersion)

  implementation("ch.qos.logback:logback-classic:1.2.3")
}

kotlin {
  jvmToolchain {
    languageVersion.set(JavaLanguageVersion.of(21))
    vendor.set(JvmVendorSpec.AMAZON)
  }
}
