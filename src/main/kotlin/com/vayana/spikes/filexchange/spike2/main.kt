package com.vayana.spikes.filexchange.spike2

import arrow.core.*
import arrow.core.computations.either
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.actor
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.file.Path
import java.nio.file.Paths
import java.time.Instant
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlin.collections.mapOf

val LOG: Logger = LoggerFactory.getLogger("com.vayana.spikes.filexchange.spike2")

// -- Utils
fun <T> createActor(cs: CoroutineScope, fn: suspend (T) -> Unit): SendChannel<T> =
  cs.actor<T>(capacity = Channel.BUFFERED) {
    for (msg in channel)
      fn(msg)
  }

inline class Guid(val uuidString: String = UUID.randomUUID().toString())

interface DomainError
data class SomeError(val msg: String) : DomainError

data class Metadata(val correlationId: Guid, val sequence: Int, val timestampMillis: Long = Instant.now().toEpochMilli())

interface Event {
  val metadata: Metadata
}

sealed class FileXChangeEvent : Event
data class FileReceived(override val metadata: Metadata, val localPath: Path, val fileName: String, val senderUser: String, val senderEntity: String) : FileXChangeEvent()
data class VirusChecked(override val metadata: Metadata, val virusInfected: Boolean) : FileXChangeEvent()
data class MimeTypeDetected(override val metadata: Metadata, val mimeType: String) : FileXChangeEvent()
data class StoredInS3(override val metadata: Metadata, val uniqueName: String, val s3Location: String) : FileXChangeEvent()
data class FileApproved(override val metadata: Metadata, val approved: Boolean=true) : FileXChangeEvent()
data class FileRejected(override val metadata: Metadata, val approved: Boolean=false) : FileXChangeEvent()
data class FileProcessingFailed(override val metadata: Metadata, val stage: String, val errorMsg: String): FileXChangeEvent()

typealias Events<T> = List<T>

interface EventMsg<E: Event>

typealias EventProcessorResult<E> = Either<DomainError, Events<E>>
typealias EventProcessor<E> = (Guid, Events<E>) -> EventProcessorResult<E>
fun <E: Event> processEvents(correlationId: Guid, events: Events<E>, eventProcessorFn: EventProcessor<E>): EventProcessorResult<E> = eventProcessorFn(correlationId, events)

interface EventStore<E: Event, EM: EventMsg<E>> {
  fun get(correlationId: Guid): Events<E>
  fun append(correlationId: Guid, events: Events<E>): Unit
  fun evolve(correlationId: Guid, eventProcessor: EventProcessor<E>): Option<DomainError>
}

interface EventStoreActor<E: Event, EM: EventMsg<E>>: EventStore<E, EM> {
  val channel: SendChannel<EM>
  suspend fun appendEvents(correlationId: Guid, events: Events<E>): Unit
  suspend fun getEvents(correlationId: Guid, response: CompletableDeferred<Events<FileXChangeEvent>>): Unit
  suspend fun receiveFile(correlationId: Guid, localPath: Path, fileName: String, senderUser: String, senderEntity: String, eventProcessor: EventProcessor<E>): Unit
  suspend fun approveFile(correlationId: Guid, eventProcessor: EventProcessor<E>): Unit
  suspend fun rejectFile(correlationId: Guid, eventProcessor: EventProcessor<E>): Unit
  suspend fun getAllEvents(response: CompletableDeferred<Events<FileXChangeEvent>>): Unit
  fun stop(): Boolean
}

sealed class FileXChangeEventStoreMsg: EventMsg<FileXChangeEvent>
data class GetFileXChangeEventsFor(val correlationId: Guid, val response: CompletableDeferred<Events<FileXChangeEvent>>): FileXChangeEventStoreMsg()
data class AppendFileXChangeEventsFor(val correlationId: Guid, val events: Events<FileXChangeEvent>): FileXChangeEventStoreMsg()
data class DoReceiveFile(val correlationId: Guid, val localPath: Path, val fileName: String, val senderUser: String, val senderEntity: String, val eventProcessor: EventProcessor<FileXChangeEvent>): FileXChangeEventStoreMsg()
data class DoApproveFile(val correlationId: Guid, val eventProcessor: EventProcessor<FileXChangeEvent>): FileXChangeEventStoreMsg()
data class DoRejectFile(val correlationId: Guid, val eventProcessor: EventProcessor<FileXChangeEvent>): FileXChangeEventStoreMsg()
data class GetAllReceivedFiles(val response: CompletableDeferred<Map<FileIdentifier, FileDetails>>): FileXChangeEventStoreMsg()
data class GetAllEvents(val response: CompletableDeferred<Events<FileXChangeEvent>>): FileXChangeEventStoreMsg()

class InMemoryFileXChangeEventStore(val cs: CoroutineScope): EventStoreActor<FileXChangeEvent, FileXChangeEventStoreMsg> {
  override val channel: SendChannel<FileXChangeEventStoreMsg> = createActor(cs) { processEventMsg(it) }
  private val store = ConcurrentHashMap<Guid, List<FileXChangeEvent>>()

  private fun processEventMsg(msg: FileXChangeEventStoreMsg) =
    when(msg) {
      is GetFileXChangeEventsFor -> msg.response.complete(get(msg.correlationId))
      is AppendFileXChangeEventsFor -> append(msg.correlationId, msg.events)
      is GetAllReceivedFiles -> msg.response.complete(
        ReceivedFilesProjection().let { projection ->
          store.values.flatMap { evs ->
            evs.filter { ev -> ev.javaClass == FileReceived::class.java }
          }.fold(projection.initial) { s, e -> projection.update(s, e) }
        }
      )
      is DoReceiveFile -> evolve(msg.correlationId, msg.eventProcessor) //TODO: The error if any is lost here
      is DoApproveFile -> evolve(msg.correlationId, msg.eventProcessor) //TODO: The error if any is lost here
      is DoRejectFile -> evolve(msg.correlationId, msg.eventProcessor)  //TODO: The error if any is lost here
      is GetAllEvents -> msg.response.complete(GetAllEventsProjection().let { projection ->
        store.values.flatten().fold(projection.initial) { s, e -> projection.update(s, e) }
      })
    }

  override fun get(correlationId: Guid): Events<FileXChangeEvent> = store.getOrPut(correlationId) { emptyList() }

  override fun append(correlationId: Guid, events: Events<FileXChangeEvent>): Unit {
    store[correlationId] = get(correlationId) + events
  }

  override fun evolve(correlationId: Guid, eventProcessor: EventProcessor<FileXChangeEvent>): Option<DomainError> =
    processEvents(correlationId, get(correlationId), eventProcessor).fold( { it.some() }, { append(correlationId, it); None })

  override suspend fun appendEvents(correlationId: Guid, events: Events<FileXChangeEvent>) = channel.send(AppendFileXChangeEventsFor(correlationId, events))
  override suspend fun getEvents(correlationId: Guid, response: CompletableDeferred<Events<FileXChangeEvent>>) = channel.send(GetFileXChangeEventsFor(correlationId, response))
  override suspend fun receiveFile(correlationId: Guid, localPath: Path, fileName: String, senderUser: String, senderEntity: String, eventProcessor: EventProcessor<FileXChangeEvent>) {
    processEvents(correlationId, listOf(FileReceived(Metadata(correlationId, 1), localPath, fileName, senderUser, senderEntity)), eventProcessor).map {
      append(correlationId, it)
    }
  }
  override suspend fun getAllEvents(response: CompletableDeferred<Events<FileXChangeEvent>>) = channel.send(GetAllEvents(response))
  override suspend fun approveFile(correlationId: Guid, eventProcessor: EventProcessor<FileXChangeEvent>) {
    withContext(Dispatchers.IO) {
      val events = get(correlationId)
      processEvents(correlationId, events, eventProcessor).map {
        append(correlationId, it)
      }
    }
  }
  override suspend fun rejectFile(correlationId: Guid, eventProcessor: EventProcessor<FileXChangeEvent>) {
    processEvents(correlationId, get(correlationId), eventProcessor).map {
      append(correlationId, it)
    }
  }
  override fun stop() = channel.close()
}

interface Projection<S, E: Event> {
  val initial: S
  fun update(st: S, ev: E): S
}

typealias FileName = String
typealias FileIdentifier = Pair<Guid, FileName>
data class FileDetails(val localPath: Path, val fileName: String, val senderUser: String, val senderEntity: String)

class ReceivedFilesProjection: Projection<Map<FileIdentifier, FileDetails>, FileXChangeEvent> {
  override val initial: Map<FileIdentifier, FileDetails> = mapOf()
  override fun update(st: Map<FileIdentifier, FileDetails>, ev: FileXChangeEvent): Map<FileIdentifier, FileDetails> =
    when(ev) {
      is FileReceived -> st + mapOf(Pair(ev.metadata.correlationId, ev.fileName) to FileDetails(ev.localPath, ev.fileName, ev.senderUser, ev.senderEntity))
      else -> st
    }
}

class GetAllEventsProjection: Projection<Events<FileXChangeEvent>, FileXChangeEvent> {
  override val initial: Events<FileXChangeEvent> = listOf()
  override fun update(st: Events<FileXChangeEvent>, ev: FileXChangeEvent): Events<FileXChangeEvent> = st + listOf(ev)
}

private fun performVirusCheck(correlationId: Guid, localPath: Path, fileName: String): Either<DomainError, VirusChecked> =
  VirusChecked(Metadata(correlationId, 2), false).right()

private fun detectMimeType(correlationId: Guid, localPath: Path, fileName: String): Either<DomainError, MimeTypeDetected> =
  MimeTypeDetected(Metadata(correlationId, 3), "ms/excel").right()

private fun storeInS3(correlationId: Guid, localPath: Path, fileName: String): Either<DomainError, StoredInS3> =
  StoredInS3(Metadata(correlationId, 4), "unique-$fileName", "vayana-prod/incoming").right()

private fun approveIfOk(correlationId: Guid): Either<DomainError, FileApproved> =
  FileApproved(Metadata(correlationId, 5), true).right()

fun processFileReceivedEvent(correlationId: Guid, events: Events<FileXChangeEvent>): EventProcessorResult<FileXChangeEvent> {
  val eventOfInterest = Option.fromNullable(events.firstOrNull {
    it.metadata.correlationId == correlationId && it.javaClass == FileReceived::class.java
  }) as Option<FileReceived>

  return eventOfInterest.fold(
    { SomeError("No event found").left() },
    { event ->
      either.eager<DomainError, Events<FileXChangeEvent>> {
        val e1 = FileReceived(Metadata(event.metadata.correlationId, 1), event.localPath, event.fileName, event.senderUser, event.senderEntity)
        val e2 = performVirusCheck(event.metadata.correlationId, event.localPath, event.fileName).bind()
        val e3 = detectMimeType(event.metadata.correlationId, event.localPath, event.fileName).bind()
        val e4 = storeInS3(event.metadata.correlationId, event.localPath, event.fileName).bind()
        listOf<FileXChangeEvent>(e1, e2, e3, e4)
      }
    }
  )
}

fun processFileReceivedEventError(correlationId: Guid, events: Events<FileXChangeEvent>): EventProcessorResult<FileXChangeEvent> {
  val eventOfInterest = Option.fromNullable(events.firstOrNull {
    it.metadata.correlationId == correlationId && it.javaClass == FileReceived::class.java
  }) as Option<FileReceived>

  return eventOfInterest.fold(
    { SomeError("No event found").left() },
    { event ->
      either.eager<DomainError, Events<FileXChangeEvent>> {
        val e1 = FileReceived(Metadata(event.metadata.correlationId, 1), event.localPath, event.fileName, event.senderUser, event.senderEntity)
        val e2 = performVirusCheck(event.metadata.correlationId, event.localPath, event.fileName).bind()
        val e3 = FileProcessingFailed(Metadata(event.metadata.correlationId, 3), "upload", "Forced error")
        listOf<FileXChangeEvent>(e1, e2, e3)
      }
    }
  )
}

fun processFileApprovedEvent(correlationId: Guid, events: Events<FileXChangeEvent>): EventProcessorResult<FileXChangeEvent> {
  val lastEvent = Option.fromNullable(events.last())
  return lastEvent.fold(
    {
      val msg = "File referred by $correlationId not found for approval processing"
      LOG.error(msg)
      SomeError(msg).left()
    },
    { event ->
      when (event) {
        is StoredInS3 -> listOf(FileApproved(Metadata(correlationId, 5))).right()
        else -> listOf(FileProcessingFailed(Metadata(correlationId, events.size+1), "approval", "File not found in S3")).right()
      }
    }
  )
}

fun processFileRejectedEvent(correlationId: Guid, events: Events<FileXChangeEvent>): EventProcessorResult<FileXChangeEvent> {
  val lastEvent = Option.fromNullable(events.last())
  return lastEvent.fold(
    {
      val msg = "File referred by $correlationId not found for approval processing"
      LOG.error(msg)
      SomeError(msg).left()
    },
    { event ->
      when (event) {
        //TODO: if rejection happens any time after storing in s3, then we need a logic to compute the right sequence
        is StoredInS3 -> listOf(FileRejected(Metadata(correlationId, 5))).right()
        else -> listOf(FileProcessingFailed(Metadata(correlationId, events.size+1), "approval", "File not found in S3")).right()
      }
    }
  )
}


fun main() {
  fun Events<FileXChangeEvent>.printAll() {
    LOG.debug("${this@printAll.size}")
    if (this@printAll.isEmpty()) LOG.debug(this@printAll.toString())
    else this@printAll.forEach { LOG.debug(it.toString()) }
  }

  fun Map<FileIdentifier, FileDetails>.printAll() {
    LOG.debug("${this@printAll.size}")
    if (this@printAll.isEmpty()) LOG.debug(this@printAll.toString())
    else this@printAll.entries.forEach {(k, v) -> LOG.debug("$k => $v") }
  }

  val dash10 = "--".repeat(5)
  val dash25 = "-----".repeat(5)
  runBlocking {
    val eventStore = InMemoryFileXChangeEventStore(this)
    val correlationId1 = Guid()
    val correlationId2 = Guid()
    val correlationId3 = Guid()

    println("$dash10 Initial set of events $dash10")
    val r1 = CompletableDeferred<Events<FileXChangeEvent>>()
    eventStore.getEvents(correlationId1, r1)
    r1.await().printAll()

    println("\n>>>>> Receiving multiple files\n")
    eventStore.receiveFile(correlationId1, Paths.get("/home/nkhalasi/Downloads"), "abcd.xlsx", "naresh", "vayana", ::processFileReceivedEvent)
    eventStore.receiveFile(correlationId2, Paths.get("/home/nkhalasi/Downloads"), "pqrs.xlsx", "naresh", "vayana", ::processFileReceivedEvent)
    eventStore.receiveFile(correlationId3, Paths.get("/home/nkhalasi/Downloads"), "xyz.pdf", "naresh", "vayana", ::processFileReceivedEventError)

    println("$dash10 All file receipt events $dash10")
    CompletableDeferred<Map<FileIdentifier, FileDetails>>().let{
      eventStore.channel.send(GetAllReceivedFiles(it))
      it.await().printAll()
    }

    println("$dash10 Events related to $correlationId2 $dash10")
    val r2 = CompletableDeferred<Events<FileXChangeEvent>>()
    eventStore.getEvents(correlationId2, r2)
    r2.await().printAll()

    println("$dash10 All events currently in event store $dash10")
    CompletableDeferred<Events<FileXChangeEvent>>().let {
      eventStore.getAllEvents(it)
      it.await().printAll()
    }

    println("\n>>>>> Approving $correlationId1 & rejecting $correlationId2, $correlationId3\n")
    eventStore.approveFile(correlationId1, ::processFileApprovedEvent)
    eventStore.rejectFile(correlationId2, ::processFileRejectedEvent)
    eventStore.rejectFile(correlationId3, ::processFileRejectedEvent) //this will result into FileProcessingFailed event

    println("$dash10 All events currently in event store $dash10")
    CompletableDeferred<Events<FileXChangeEvent>>().let {
      eventStore.getAllEvents(it)
      it.await().printAll()
    }
    println("$dash25")

    eventStore.stop()
  }
}
