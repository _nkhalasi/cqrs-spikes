package com.vayana.spikes.filexchange.spike1

import arrow.core.*
import arrow.core.computations.either
import java.nio.file.Path
import java.nio.file.Paths
import java.time.Instant
import java.util.*
import java.util.concurrent.ConcurrentHashMap

inline class Guid(val uuidString: String = UUID.randomUUID().toString())

// -- Commands
interface Command {
  fun correlationId(): Guid
}

sealed class FileXChangeCommand : Command {
  abstract val fileId: Guid
  override fun correlationId(): Guid = fileId
}

data class ReceiveFile(
  override val fileId: Guid,
  val localPath: Path,
  val fileName: String,
  val senderUser: String,
  val senderEntity: String
) : FileXChangeCommand()

data class ApproveForProcessing(override val fileId: Guid) : FileXChangeCommand()
data class ProcessFile(override val fileId: Guid) : FileXChangeCommand()

sealed class NullCommand : Command {
  abstract val id: Guid
  override fun correlationId(): Guid = id
}

data class NoopCommand(override val id: Guid = Guid()) : NullCommand()

// -- Events
interface Event {
  val metadata: Metadata
}

data class Metadata(
  val correlationId: Guid,
  val sequence: Int,
  val timestampMillis: Long = Instant.now().toEpochMilli()
)

//TODO: How do we handle sequences if we need to introduce new events between two existing event in the future?
//data class Envelope<T: Event>(val metadata: Metadata, val payload: T)
sealed class FileXChangeEvent : Event

data class FileReceived(
  override val metadata: Metadata,
  val localPath: Path,
  val fileName: String,
  val senderUser: String,
  val senderEntity: String
) : FileXChangeEvent()

data class VirusChecked(override val metadata: Metadata, val virusInfected: Boolean) : FileXChangeEvent()
data class MimeTypeDetected(override val metadata: Metadata, val mimeType: String) : FileXChangeEvent()
data class StoredInS3(override val metadata: Metadata, val uniqueName: String, val s3Location: String) :
  FileXChangeEvent()

data class FileApproved(override val metadata: Metadata, val approved: Boolean) : FileXChangeEvent()

// -- State Models
interface State

sealed class FileXChangeState : State {
  abstract val correlationId: Guid
}

data class NewFile(override val correlationId: Guid) : FileXChangeState()
data class ReceivedFile(
  override val correlationId: Guid,
  val fileName: String,
  val uniqueName: String,
  val senderUser: String,
  val senderEntity: String
) : FileXChangeState()

data class VirusCheckedFile(
  override val correlationId: Guid,
  val fileName: String,
  val uniqueName: String,
  val senderUser: String,
  val senderEntity: String,
  val virusInfected: Boolean
) : FileXChangeState()

data class MimetypeDetectedFile(
  override val correlationId: Guid,
  val fileName: String,
  val uniqueName: String,
  val senderUser: String,
  val senderEntity: String,
  val virusInfected: Boolean,
  val mimeType: String
) : FileXChangeState()

data class StoredInS3File(
  override val correlationId: Guid,
  val fileName: String,
  val uniqueName: String,
  val senderUser: String,
  val senderEntity: String,
  val virusInfected: Boolean,
  val mimeType: String,
  val s3Location: String
) : FileXChangeState()

data class ApprovedFile(
  override val correlationId: Guid,
  val fileName: String,
  val uniqueName: String,
  val senderUser: String,
  val senderEntity: String,
  val virusInfected: Boolean,
  val mimeType: String,
  val s3Location: String,
  val approved: Boolean
) : FileXChangeState()

sealed class NullEvent : Event
data class NoopEvent(override val metadata: Metadata) : NullEvent()

// -- Aggregates - Command Handlers
interface DomainError

data class SomeError(val msg: String) : DomainError

typealias CmdResult<TEvent> = Either<DomainError, List<TEvent>> //cmd result is either an error or list of events

private fun performVirusCheck(
  correlationId: Guid,
  localPath: Path,
  fileName: String
): Either<DomainError, VirusChecked> =
  VirusChecked(Metadata(correlationId, 2), false).right()

private fun detectMimeType(
  correlationId: Guid,
  localPath: Path,
  fileName: String
): Either<DomainError, MimeTypeDetected> =
  MimeTypeDetected(Metadata(correlationId, 3), "ms/excel").right()

private fun storeInS3(correlationId: Guid, localPath: Path, fileName: String): Either<DomainError, StoredInS3> =
  StoredInS3(Metadata(correlationId, 4), "unique-$fileName", "vayana-prod/incoming").right()

private fun approveIfOk(correlationId: Guid): Either<DomainError, FileApproved> =
  FileApproved(Metadata(correlationId, 5), true).right()

private fun handle(cmd: FileXChangeCommand): CmdResult<FileXChangeEvent> =
  when (cmd) {
    is ReceiveFile -> {
      either.eager<DomainError, List<FileXChangeEvent>> {
        val e1 =
          FileReceived(Metadata(cmd.correlationId(), 1), cmd.localPath, cmd.fileName, cmd.senderUser, cmd.senderEntity)
        val e2 = performVirusCheck(cmd.correlationId(), cmd.localPath, cmd.fileName).bind()
        val e3 = detectMimeType(cmd.correlationId(), cmd.localPath, cmd.fileName).bind()
        val e4 = storeInS3(cmd.correlationId(), cmd.localPath, cmd.fileName).bind()
        listOf<FileXChangeEvent>(e1, e2, e3, e4)
      }
    }
    is ApproveForProcessing -> {
      either.eager<DomainError, List<FileXChangeEvent>> {
        val e1 = approveIfOk(cmd.correlationId()).bind()
        listOf(e1)
      }
    }
    is ProcessFile -> {
      //continue from where it left off: load events, identify the last event and based on that generate the rest of the events
      listOf<FileXChangeEvent>().right()
    }
  }

private fun handle(cmd: NullCommand): CmdResult<NullEvent> =
  when (cmd) {
    is NoopCommand -> listOf<NoopEvent>().right()
  }

fun handleFileXChangeCommand(cmd: FileXChangeCommand): CmdResult<FileXChangeEvent> = handle(cmd)
fun handleNullCommand(cmd: NullCommand): CmdResult<NullEvent> = handle(cmd)

typealias CmdHandler<TCommand, TEvent> = (TCommand) -> CmdResult<TEvent>  //cmd handler takes a cmd and gives back cmd result

fun <TCommand : Command, TEvent : Event> process(
  cmd: TCommand,
  block: CmdHandler<TCommand, TEvent>
): CmdResult<TEvent> = block(cmd)

// -- Event Store
interface EventStream : Iterable<Event> {
  val version: Int
  fun <TEvent : Event> append(newEvents: List<TEvent>): EventStream
}

class InMemoryEventStream(val events: List<Event> = emptyList(), override val version: Int = 0) : EventStream {
  override fun iterator(): Iterator<Event> = events.iterator()
  override fun <TEvent : Event> append(newEvents: List<TEvent>): InMemoryEventStream =
    InMemoryEventStream(events + newEvents, version + 1)
}

interface EventStore {
  fun loadEventStream(correlationId: Guid): EventStream
  fun <TEvent : Event> store(correlationId: Guid, version: Int, events: List<TEvent>): Option<DomainError>
}

class InMemoryEventStore : EventStore {
  private val streams = ConcurrentHashMap<Guid, InMemoryEventStream>()
  override fun loadEventStream(correlationId: Guid): InMemoryEventStream =
    streams.getOrPut(correlationId) { InMemoryEventStream() }

  override fun <TEvent : Event> store(correlationId: Guid, version: Int, newEvents: List<TEvent>): Option<DomainError> =
    loadEventStream(correlationId).let { es ->
      if (es.version != version)
        SomeError("Stream has already been modified").some()
      else {
        streams[correlationId] = es.append(newEvents)
        None
      }
    }
}

// -- Given a state and command, process and get a list of events
typealias ExecuteCommandFn<TState, TCommand, TEvent> = (TState, TCommand) -> CmdResult<TEvent>

fun <TState : State, TCommand : Command, TEvent : Event> executeCommand(
  state: TState,
  cmd: TCommand,
  block: ExecuteCommandFn<TState, TCommand, TEvent>
): CmdResult<TEvent> =
  block(state, cmd)

fun executeFileXChangeCommand(st: FileXChangeState, cmd: FileXChangeCommand): CmdResult<FileXChangeEvent> =
  when {
    st is NewFile && cmd is ReceiveFile -> process(cmd, ::handleFileXChangeCommand)
    st is StoredInS3File && cmd is ApproveForProcessing -> process(cmd, ::handleFileXChangeCommand)
    else -> SomeError("$cmd not applicable on $st").left()
  }

// -- Apply an event to a state and get the new state
typealias ApplyEventFn<TState, TEvent> = (TState, TEvent) -> TState

fun <TState : State, TEvent : Event> applyEvent(
  state: TState,
  event: TEvent,
  block: ApplyEventFn<TState, TEvent>
): TState =
  block(state, event)

fun applyFileXChangeEvent(state: FileXChangeState, event: FileXChangeEvent): FileXChangeState =
  when {
    state is NewFile && event is FileReceived -> ReceivedFile(
      event.metadata.correlationId,
      event.fileName,
      "unique-${event.fileName}",
      event.senderUser,
      event.senderEntity
    )
    state is ReceivedFile && event is VirusChecked -> VirusCheckedFile(
      state.correlationId,
      state.fileName,
      state.uniqueName,
      state.senderUser,
      state.senderEntity,
      event.virusInfected
    )
    state is VirusCheckedFile && event is MimeTypeDetected -> MimetypeDetectedFile(
      state.correlationId,
      state.fileName,
      state.uniqueName,
      state.senderUser,
      state.senderEntity,
      state.virusInfected,
      event.mimeType
    )
    state is MimetypeDetectedFile && event is StoredInS3 -> StoredInS3File(
      state.correlationId,
      state.fileName,
      state.uniqueName,
      state.senderUser,
      state.senderEntity,
      state.virusInfected,
      state.mimeType,
      event.s3Location
    )
    state is StoredInS3File && event is FileApproved -> ApprovedFile(
      state.correlationId,
      state.fileName,
      state.uniqueName,
      state.senderUser,
      state.senderEntity,
      state.virusInfected,
      state.mimeType,
      state.s3Location,
      event.approved
    )
    else -> state //TODO: Do we need a better error handling? This is an invalid combination scenario.
  }

typealias ApplyEventsFn<TState, TEvent> = (TState, List<TEvent>) -> TState

fun <TState : State, TEvent : Event> applyEvents(
  state: TState,
  events: List<TEvent>,
  block: ApplyEventsFn<TState, TEvent>
): TState =
  block(state, events)

fun applyFileXChangeEvents(initialState: FileXChangeState, events: List<FileXChangeEvent>): FileXChangeState =
  events.fold(initialState, ::applyFileXChangeEvent)

// -- Aggregates
interface Aggregate<TState : State, TCommand : Command, TEvent : Event> {
  val initialState: TState
  fun apply(st: TState, ev: TEvent): TState
  fun execute(st: TState, cmd: TCommand): CmdResult<TEvent>
}

class FileXChangeAggregate(val es: InMemoryEventStore) :
  Aggregate<FileXChangeState, FileXChangeCommand, FileXChangeEvent> {
  override val initialState: FileXChangeState = NewFile(Guid(""))

  override fun apply(st: FileXChangeState, ev: FileXChangeEvent): FileXChangeState =
    applyEvent(st, ev, ::applyFileXChangeEvent)

//  override fun apply(st: FileXChangeState, evs: List<FileXChangeEvent>): FileXChangeState =
//    applyEvents(st, evs, ::applyFileXChangeEvents)

  override fun execute(st: FileXChangeState, cmd: FileXChangeCommand): CmdResult<FileXChangeEvent> {
    val eventStream = es.loadEventStream(cmd.correlationId())
    val events = eventStream.events as List<FileXChangeEvent> //TODO: How can we eliminate this type casting?
    val currentState = applyEvents(initialState, events, ::applyFileXChangeEvents)
    return executeCommand(currentState, cmd, ::executeFileXChangeCommand).flatMap {
      es.store(cmd.correlationId(), eventStream.version, it)
      it.right()
    }
  }
}

fun main() {
  val db = InMemoryEventStore()
  val agg = FileXChangeAggregate(db)
  val correlationId = Guid()
  val cmd1 = ReceiveFile(correlationId, Paths.get("/home/nkhalasi/Downloads"), "abcd.xlsx", "naresh", "vayana")
  println(cmd1.correlationId())
  println("-----".repeat(5))
  agg.execute(NewFile(Guid("")), cmd1)
  db.loadEventStream(correlationId).events.forEach { println(it) }
  println("--Sleeping for 3 secs--")
  Thread.sleep(3000)
  val cmd2 = ApproveForProcessing(correlationId)
  agg.execute(NewFile(Guid("")), cmd2)
  db.loadEventStream(correlationId).events.forEach { println(it) }
}
