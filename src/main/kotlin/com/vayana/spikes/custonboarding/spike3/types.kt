package com.vayana.spikes.custonboarding.spike3

import java.time.LocalDate

//Events
sealed class CustomerApplicationEvent: Event {
  abstract override val metadata: Metadata
}
data class CustomerApplicationSubmitted(
  override val metadata: Metadata,
  val customerName: String,
  val customerAddress: String,
  val customerPhone: String,
  val customerEmail: String,
): CustomerApplicationEvent()
data class CustomerApplicationUpdated(
  override val metadata: Metadata,
  val customerName: String,
  val customerAddress: String,
  val customerPhone: String,
  val customerEmail: String
): CustomerApplicationEvent()
data class CustomerApplicationApproved(
  override val metadata: Metadata,
  val approvedBy: String
): CustomerApplicationEvent()
data class CustomerApplicationRejected(
  override val metadata: Metadata,
  val rejectedBy: String
): CustomerApplicationEvent()

//Application Data
data class CustomerApplication(
  val customerName: String,
  val customerAddress: String,
  val customerPhone: String,
  val customerEmail: String,
  val appliedOn: LocalDate
)
enum class ApplicationStatus {
  New,
  BeingReviewed,
  SentBack,
  Approved,
  Rejected
}

//States
sealed class CustomerApplicationState: State {
  abstract override val correlationId: Guid
  abstract val applicationStatus: ApplicationStatus
}
data class NewCustomerApplication(
  override val correlationId: Guid,
  override val applicationStatus: ApplicationStatus = ApplicationStatus.New
): CustomerApplicationState()
data class UnderProcessingCustomerApplication(
  override val correlationId: Guid,
  val applicationData: CustomerApplication,
  override val applicationStatus: ApplicationStatus = ApplicationStatus.BeingReviewed
): CustomerApplicationState()
data class CustomerApplicationBeingUpdated(
  override val correlationId: Guid,
  val applicationData: CustomerApplication,
  val sentBackOn: LocalDate,
  override val applicationStatus: ApplicationStatus = ApplicationStatus.SentBack
): CustomerApplicationState()
data class ApprovedCustomerApplication(
  override val correlationId: Guid,
  val applicationData: CustomerApplication,
  val approvedBy: String,
  val approvedOn: LocalDate,
  override val applicationStatus: ApplicationStatus = ApplicationStatus.Approved
): CustomerApplicationState()
data class RejectedCustomerApplication(
  override val correlationId: Guid,
  val applicationData: CustomerApplication,
  val rejectedBy: String,
  val rejectedOn: LocalDate,
  override val applicationStatus: ApplicationStatus = ApplicationStatus.Rejected
): CustomerApplicationState()


//Commands
sealed class CustomerApplicationCmd: Command {
  abstract val applicationId: Guid
  override fun correlationId() = applicationId
}
data class AcceptCustomerApplication(
  override val applicationId: Guid,
  val customerName: String,
  val customerAddress: String,
  val customerPhone: String,
  val customerEmail: String
): CustomerApplicationCmd()
data class UpdateCustomerApplication(
  override val applicationId: Guid,
  val customerName: String,
  val customerAddress: String,
  val customerPhone: String,
  val customerEmail: String
): CustomerApplicationCmd()
data class ApproveCustomerApplication(
  override val applicationId: Guid,
  val approvingUser: String
): CustomerApplicationCmd()
data class RejectCustomerApplication(
  override val applicationId: Guid,
  val rejectingUser: String
): CustomerApplicationCmd()

//Command Context
data class NoopCmdContext(
  val nothing: String = "dummy"
): CommandContext
