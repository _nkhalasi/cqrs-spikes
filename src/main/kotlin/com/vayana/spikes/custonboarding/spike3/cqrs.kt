package com.vayana.spikes.custonboarding.spike3


import arrow.core.Either

import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.util.*

inline class Guid(val uuidString: String = UUID.randomUUID().toString())

// -- Commands
interface Command {
  fun correlationId(): Guid
}

interface CommandContext

// -- Events
interface Event {
  val metadata: Metadata
}

data class Metadata(val correlationId: Guid, val sequence: Int = 0, val timestampMillis: Long = Instant.now().toEpochMilli()) {
  val actionDate: LocalDate = LocalDate.ofInstant(
    Instant.ofEpochMilli(timestampMillis),
    ZoneId.of("Asia/Kolkata")
  )
}

// -- State Models
interface State {
  val correlationId: Guid
}

interface DomainError
data class SomeError(val msg: String) : DomainError

// -- Command Handlers
typealias CmdResult<TEvent> = Either<DomainError, List<TEvent>> //cmd result is either an error or list of events

typealias CmdHandler<TCommand, TEvent> = (TCommand) -> CmdResult<TEvent>  //cmd handler takes a cmd and gives back cmd result

// -- Given a state and command, process and get a list of events
typealias ExecuteCommandFn<TState, TCommand, TCommandCtx, TEvent> =  (TState, TCommand, TCommandCtx) -> CmdResult<TEvent>

fun <TState : State, TCommand : Command, TCommandCtx : CommandContext, TEvent : Event> executeCommand(
  state: TState,
  cmd: TCommand,
  cmdCtx: TCommandCtx,
  block: ExecuteCommandFn<TState, TCommand, TCommandCtx, TEvent>
): CmdResult<TEvent> = block(state, cmd, cmdCtx)

// -- Apply an event to a state and get the new state
typealias ApplyEventFn<TState, TEvent> = (TState, TEvent) -> TState

fun <TState : State, TEvent : Event> applyEvent(state: TState, event: TEvent, block: ApplyEventFn<TState, TEvent>): TState =
  block(state, event)

fun <TState : State, TEvent : Event> applyEvents(state: TState, events: List<TEvent>, block: ApplyEventFn<TState, TEvent>): TState =
  events.fold(state, block)

// -- Aggregates
interface Aggregate<TState : State, TCommand : Command, TCommandCtx : CommandContext, TEvent : Event> {
  val initialState: TState
  fun execute(st: TState, cmd: TCommand, cmdCtx: TCommandCtx): CmdResult<TEvent>
  fun apply(st: TState, ev: TEvent): TState
  fun apply(st: TState, evs: List<TEvent>): TState
}
