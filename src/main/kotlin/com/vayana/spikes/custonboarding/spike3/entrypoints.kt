package com.vayana.spikes.custonboarding.spike3

import arrow.core.computations.either
import arrow.core.left

fun handleCustomerApplicationCmd(
  st: CustomerApplicationState,
  cmd: CustomerApplicationCmd,
  ctx: CommandContext
): CmdResult<CustomerApplicationEvent> =
  when(cmd) {
    is AcceptCustomerApplication ->
      either.eager {
        listOf(
          CustomerApplicationSubmitted(
            Metadata(Guid(cmd.applicationId.toString())),
            cmd.customerName,
            cmd.customerAddress,
            cmd.customerPhone,
            cmd.customerEmail
          )
        )
      }
    is UpdateCustomerApplication ->
      either.eager {
        listOf(
          CustomerApplicationUpdated(
            Metadata(Guid(cmd.applicationId.toString())),
            cmd.customerName,
            cmd.customerAddress,
            cmd.customerPhone,
            cmd.customerEmail
          )
        )
      }
    is ApproveCustomerApplication ->
      either.eager {
        listOf(
          CustomerApplicationApproved(
            Metadata(Guid(cmd.applicationId.toString())),
            cmd.approvingUser
          )
        )
      }
    is RejectCustomerApplication ->
      either.eager {
        listOf(
          CustomerApplicationRejected(
            Metadata(Guid(cmd.applicationId.toString())),
            cmd.rejectingUser
          )
        )
      }
  }


fun executeCustomerApplicationCmd(
  st: CustomerApplicationState,
  cmd: CustomerApplicationCmd,
  ctx: CommandContext
): CmdResult<CustomerApplicationEvent> =
  when {
    st is NewCustomerApplication && cmd is AcceptCustomerApplication ->
      executeCommand(st, cmd, NoopCmdContext(), ::handleCustomerApplicationCmd)
    st is UnderProcessingCustomerApplication && cmd is ApproveCustomerApplication ->
      executeCommand(st, cmd, NoopCmdContext(), ::handleCustomerApplicationCmd)
    st is UnderProcessingCustomerApplication && cmd is RejectCustomerApplication ->
      executeCommand(st, cmd, NoopCmdContext(), ::handleCustomerApplicationCmd)
    else ->
      SomeError("action not allowed").left()
  }
