package com.vayana.spikes.custonboarding.spike3

import arrow.core.flatMap
import arrow.core.right
import java.util.*

fun applyCustomerApplicationEvent(
  st: CustomerApplicationState,
  ev: CustomerApplicationEvent
): CustomerApplicationState =
  when {
    st is NewCustomerApplication && ev is CustomerApplicationSubmitted ->
      UnderProcessingCustomerApplication(
        ev.metadata.correlationId,
        CustomerApplication(
          ev.customerName,
          ev.customerAddress,
          ev.customerPhone,
          ev.customerEmail,
          ev.metadata.actionDate
        )
      )
    st is UnderProcessingCustomerApplication && ev is CustomerApplicationApproved ->
      ApprovedCustomerApplication(
        ev.metadata.correlationId,
        st.applicationData,
        ev.approvedBy,
        ev.metadata.actionDate
      )
    st is UnderProcessingCustomerApplication && ev is CustomerApplicationRejected ->
      RejectedCustomerApplication(
        ev.metadata.correlationId,
        st.applicationData,
        ev.rejectedBy,
        ev.metadata.actionDate
      )
    else -> st
  }

class CustomerApplicationAggregate(
  private val es: CustomerApplications
): Aggregate<CustomerApplicationState, CustomerApplicationCmd, CommandContext, CustomerApplicationEvent> {
  override val initialState: CustomerApplicationState = NewCustomerApplication(Guid(""))
  override fun apply(st: CustomerApplicationState, ev: CustomerApplicationEvent): CustomerApplicationState =
    applyEvent(st, ev, ::applyCustomerApplicationEvent)

  override fun apply(st: CustomerApplicationState, evs: List<CustomerApplicationEvent>): CustomerApplicationState =
    applyEvents(st, evs, ::applyCustomerApplicationEvent)

  override fun execute(
    st: CustomerApplicationState,
    cmd: CustomerApplicationCmd,
    cmdCtx: CommandContext
  ): CmdResult<CustomerApplicationEvent> {
    val eventStream = es.loadEventStream(cmd.correlationId())
    val events = eventStream.events as List<CustomerApplicationEvent>
    val currentState = apply(initialState, events)
    return executeCommand(
      currentState, cmd, NoopCmdContext(), ::executeCustomerApplicationCmd
    ).flatMap {
      es.store(cmd.correlationId(), it)
      it.right()
    }
  }
}

fun main() {
  val db = CustomerApplications()
  val cmdCtx = NoopCmdContext()
  val correlationId = Guid(UUID.randomUUID().toString())
  val agg = CustomerApplicationAggregate(db)

  //Simulate the system usage
  val cmd1 = AcceptCustomerApplication(correlationId, "John Doe", "Vadodara", "john@doe.com", "+91-9988776655")
  agg.execute(NewCustomerApplication(Guid("")), cmd1, cmdCtx)

  print("System state after submitting the application -> ")
  //Finding the current state of an application
  val currentState1 = agg.apply(
    NewCustomerApplication(Guid("")),
    db.loadEventStream(correlationId).events as List<CustomerApplicationEvent>
  )
  println(currentState1)
  println()

  val cmd2 = ApproveCustomerApplication(correlationId, "Jane Doe")
  agg.execute(NewCustomerApplication(Guid("")), cmd2, cmdCtx)

  println("System state after approving the application -> ")
  //Finding the current state of an application
  val currentState = agg.apply(
    NewCustomerApplication(Guid("")),
    db.loadEventStream(correlationId).events as List<CustomerApplicationEvent>
  )
  println(currentState)
}
