package com.vayana.spikes.custonboarding.spike3

import java.util.concurrent.ConcurrentHashMap

//Simulate the database

interface EventStream : Iterable<Event> {
  fun <TEvent: Event> append(newEvents: List<TEvent>): EventStream
}

class CustomerApplicationEvents(val events: List<Event> = emptyList()): EventStream {
  override fun <TEvent: Event> append(newEvents: List<TEvent>): CustomerApplicationEvents =
    CustomerApplicationEvents(events+newEvents)

  override fun iterator(): Iterator<Event> = events.iterator()
}

interface EventStore {
  fun loadEventStream(correlationId: Guid): EventStream
  fun <TEvent: Event> store(correlationId: Guid, events: List<TEvent>): DomainError?
}

class CustomerApplications: EventStore {
  private val applications = ConcurrentHashMap<Guid, CustomerApplicationEvents>()
  override fun loadEventStream(correlationId: Guid): CustomerApplicationEvents =
    applications.getOrPut(correlationId) { CustomerApplicationEvents() }

  override fun <TEvent: Event> store(correlationId: Guid, newEvents: List<TEvent>): DomainError? =
    loadEventStream(correlationId).let { evs ->
      applications[correlationId] = evs.append(newEvents)
      null
    }
}
