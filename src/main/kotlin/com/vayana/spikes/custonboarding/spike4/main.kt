package com.vayana.spikes.custonboarding.spike4

import arrow.core.Either
import arrow.core.computations.either
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.util.*
import java.util.concurrent.ConcurrentHashMap


//----- The Core Design Elements

inline class Guid(val uuidString: String = UUID.randomUUID().toString())
// -- Events
interface Event {
  val metadata: Metadata
}

data class Metadata(val correlationId: Guid, val sequence: Int = 0, val timestampMillis: Long = Instant.now().toEpochMilli()) {
  val actionDate: LocalDate = LocalDate.ofInstant(
    Instant.ofEpochMilli(timestampMillis),
    ZoneId.of("Asia/Kolkata")
  )
}

// -- State Models
interface State {
  val correlationId: Guid
}

// -- Commands
interface Command {
  fun correlationId(): Guid
}

interface CommandContext

interface DomainError
data class SomeError(val msg: String) : DomainError

// -- Command Handlers
typealias CmdResult<TEvent> = Either<DomainError, List<TEvent>> //cmd result is either an error or list of events

typealias CmdHandler<TCommand, TEvent> = (TCommand) -> CmdResult<TEvent>  //cmd handler takes a cmd and gives back cmd result

// -- Given a state and command, process and get a list of events
typealias ExecuteCommandFn<TState, TCommand, TCommandCtx, TEvent> =  (TState, TCommand, TCommandCtx) -> CmdResult<TEvent>

fun <TState : State, TCommand : Command, TCommandCtx : CommandContext, TEvent : Event> executeCommand(
  state: TState,
  cmd: TCommand,
  cmdCtx: TCommandCtx,
  block: ExecuteCommandFn<TState, TCommand, TCommandCtx, TEvent>
): CmdResult<TEvent> = block(state, cmd, cmdCtx)

// -- Apply an event to a state and get the new state
typealias ApplyEventFn<TState, TEvent> = (TState, TEvent) -> TState

fun <TState : State, TEvent : Event> applyEvent(state: TState, event: TEvent, block: ApplyEventFn<TState, TEvent>): TState =
  block(state, event)

fun <TState : State, TEvent : Event> applyEvents(state: TState, events: List<TEvent>, block: ApplyEventFn<TState, TEvent>): TState =
  events.fold(state, block)

// -- Aggregates
interface Aggregate<TState : State, TCommand : Command, TCommandCtx : CommandContext, TEvent : Event> {
  val initialState: TState
  fun execute(st: TState, cmd: TCommand, cmdCtx: TCommandCtx): CmdResult<TEvent>
  fun apply(st: TState, ev: TEvent): TState
  fun apply(st: TState, evs: List<TEvent>): TState
}

//------------------------------------------------------------------

//Application Data
data class CustomerApplication(
  val customerName: String,
  val customerAddress: String,
  val customerPhone: String,
  val customerEmail: String,
  val appliedOn: LocalDate
)
enum class ApplicationStatus {
  New,
  BeingReviewed,
  SentBack,
  Approved,
  Rejected
}

//------------------------------------------------------------------

//Command Context
data class NoopCmdContext(
  val nothing: String = "dummy"
): CommandContext

//Commands
sealed class CustomerApplicationCmd: Command {
  abstract val applicationId: Guid
  override fun correlationId() = applicationId
}
data class AcceptCustomerApplication(
  override val applicationId: Guid,
  val customerName: String,
  val customerAddress: String,
  val customerPhone: String,
  val customerEmail: String
): CustomerApplicationCmd()
data class UpdateCustomerApplication(
  override val applicationId: Guid,
  val customerName: String,
  val customerAddress: String,
  val customerPhone: String,
  val customerEmail: String
): CustomerApplicationCmd()
data class ApproveCustomerApplication(
  override val applicationId: Guid,
  val approvingUser: String
): CustomerApplicationCmd()
data class RejectCustomerApplication(
  override val applicationId: Guid,
  val rejectingUser: String
): CustomerApplicationCmd()

//Events
sealed class CustomerApplicationEvent: Event {
  abstract override val metadata: Metadata
}
data class CustomerApplicationSubmitted(
  override val metadata: Metadata,
  val customerName: String,
  val customerAddress: String,
  val customerPhone: String,
  val customerEmail: String,
): CustomerApplicationEvent()
data class CustomerApplicationUpdated(
  override val metadata: Metadata,
  val customerName: String,
  val customerAddress: String,
  val customerPhone: String,
  val customerEmail: String
): CustomerApplicationEvent()
data class CustomerApplicationApproved(
  override val metadata: Metadata,
  val approvedBy: String
): CustomerApplicationEvent()
data class CustomerApplicationRejected(
  override val metadata: Metadata,
  val rejectedBy: String
): CustomerApplicationEvent()

//States
sealed class CustomerApplicationState: State {
  abstract override val correlationId: Guid
  abstract val applicationStatus: ApplicationStatus
}
data class NewCustomerApplication(
  override val correlationId: Guid,
  override val applicationStatus: ApplicationStatus = ApplicationStatus.New
): CustomerApplicationState()
data class UnderProcessingCustomerApplication(
  override val correlationId: Guid,
  val applicationData: CustomerApplication,
  override val applicationStatus: ApplicationStatus = ApplicationStatus.BeingReviewed
): CustomerApplicationState()
data class CustomerApplicationBeingUpdated(
  override val correlationId: Guid,
  val applicationData: CustomerApplication,
  val sentBackOn: LocalDate,
  override val applicationStatus: ApplicationStatus = ApplicationStatus.SentBack
): CustomerApplicationState()
data class ApprovedCustomerApplication(
  override val correlationId: Guid,
  val applicationData: CustomerApplication,
  val approvedBy: String,
  val approvedOn: LocalDate,
  override val applicationStatus: ApplicationStatus = ApplicationStatus.Approved
): CustomerApplicationState()
data class RejectedCustomerApplication(
  override val correlationId: Guid,
  val applicationData: CustomerApplication,
  val rejectedBy: String,
  val rejectedOn: LocalDate,
  override val applicationStatus: ApplicationStatus = ApplicationStatus.Rejected
): CustomerApplicationState()

//------------------------------------------------------------------------
fun handleCustomerApplicationCmd(
  st: CustomerApplicationState,
  cmd: CustomerApplicationCmd,
  ctx: CommandContext
): CmdResult<CustomerApplicationEvent> =
  when(cmd) {
    is AcceptCustomerApplication ->
      either.eager {
        listOf(
          CustomerApplicationSubmitted(
            Metadata(Guid(cmd.applicationId.toString())),
            cmd.customerName,
            cmd.customerAddress,
            cmd.customerPhone,
            cmd.customerEmail
          )
        )
      }
    is UpdateCustomerApplication ->
      either.eager {
        listOf(
          CustomerApplicationUpdated(
            Metadata(Guid(cmd.applicationId.toString())),
            cmd.customerName,
            cmd.customerAddress,
            cmd.customerPhone,
            cmd.customerEmail
          )
        )
      }
    is ApproveCustomerApplication ->
      either.eager {
        listOf(
          CustomerApplicationApproved(
            Metadata(Guid(cmd.applicationId.toString())),
            cmd.approvingUser
          )
        )
      }
    is RejectCustomerApplication ->
      either.eager {
        listOf(
          CustomerApplicationRejected(
            Metadata(Guid(cmd.applicationId.toString())),
            cmd.rejectingUser
          )
        )
      }
  }


fun executeCustomerApplicationCmd(
  st: CustomerApplicationState,
  cmd: CustomerApplicationCmd,
  ctx: CommandContext
): CmdResult<CustomerApplicationEvent> =
  when {
    st is NewCustomerApplication && cmd is AcceptCustomerApplication ->
      executeCommand(st, cmd, NoopCmdContext(), ::handleCustomerApplicationCmd)
    st is UnderProcessingCustomerApplication && cmd is ApproveCustomerApplication ->
      executeCommand(st, cmd, NoopCmdContext(), ::handleCustomerApplicationCmd)
    st is UnderProcessingCustomerApplication && cmd is RejectCustomerApplication ->
      executeCommand(st, cmd, NoopCmdContext(), ::handleCustomerApplicationCmd)
    else ->
      SomeError("action not allowed").left()
  }

//------------------------------------------------------------------------
fun applyCustomerApplicationEvent(
  st: CustomerApplicationState,
  ev: CustomerApplicationEvent
): CustomerApplicationState =
  when {
    st is NewCustomerApplication && ev is CustomerApplicationSubmitted ->
      UnderProcessingCustomerApplication(
        ev.metadata.correlationId,
        CustomerApplication(
          ev.customerName,
          ev.customerAddress,
          ev.customerPhone,
          ev.customerEmail,
          ev.metadata.actionDate
        )
      )
    st is UnderProcessingCustomerApplication && ev is CustomerApplicationApproved ->
      ApprovedCustomerApplication(
        ev.metadata.correlationId,
        st.applicationData,
        ev.approvedBy,
        ev.metadata.actionDate
      )
    st is UnderProcessingCustomerApplication && ev is CustomerApplicationRejected ->
      RejectedCustomerApplication(
        ev.metadata.correlationId,
        st.applicationData,
        ev.rejectedBy,
        ev.metadata.actionDate
      )
    else -> st
  }

//------------------------------------------------------------------------
//Simulate the database

interface EventStream : Iterable<Event> {
  fun <TEvent: Event> append(newEvents: List<TEvent>): EventStream
}

class CustomerApplicationEvents(val events: List<Event> = emptyList()): EventStream {
  override fun <TEvent: Event> append(newEvents: List<TEvent>): CustomerApplicationEvents =
    CustomerApplicationEvents(events+newEvents)

  override fun iterator(): Iterator<Event> = events.iterator()
}

interface EventStore {
  fun loadEventStream(correlationId: Guid): EventStream
  fun <TEvent: Event> store(correlationId: Guid, events: List<TEvent>): DomainError?
}

class CustomerApplications: EventStore {
  private val applications = ConcurrentHashMap<Guid, CustomerApplicationEvents>()
  override fun loadEventStream(correlationId: Guid): CustomerApplicationEvents =
    applications.getOrPut(correlationId) { CustomerApplicationEvents() }

  override fun <TEvent: Event> store(correlationId: Guid, newEvents: List<TEvent>): DomainError? =
    loadEventStream(correlationId).let { evs ->
      applications[correlationId] = evs.append(newEvents)
      null
    }
}

//------------------------------------------------------------------------

class CustomerApplicationAggregate(
  private val es: CustomerApplications
): Aggregate<CustomerApplicationState, CustomerApplicationCmd, CommandContext, CustomerApplicationEvent> {
  override val initialState: CustomerApplicationState = NewCustomerApplication(Guid(""))
  override fun apply(st: CustomerApplicationState, ev: CustomerApplicationEvent): CustomerApplicationState =
    applyEvent(st, ev, ::applyCustomerApplicationEvent)

  override fun apply(st: CustomerApplicationState, evs: List<CustomerApplicationEvent>): CustomerApplicationState =
    applyEvents(st, evs, ::applyCustomerApplicationEvent)

  override fun execute(
    st: CustomerApplicationState,
    cmd: CustomerApplicationCmd,
    cmdCtx: CommandContext
  ): CmdResult<CustomerApplicationEvent> {
    val eventStream = es.loadEventStream(cmd.correlationId())
    val events = eventStream.events as List<CustomerApplicationEvent>
    val currentState = apply(initialState, events)
    return executeCommand(
      currentState, cmd, NoopCmdContext(), ::executeCustomerApplicationCmd
    ).flatMap {
      es.store(cmd.correlationId(), it)
      it.right()
    }
  }
}

//------------------------------------------------------------------------
fun main() {
  val db = CustomerApplications()
  val cmdCtx = NoopCmdContext()
  val correlationId = Guid(UUID.randomUUID().toString())
  val agg = CustomerApplicationAggregate(db)

  //Simulate the system usage
  val cmd1 = AcceptCustomerApplication(correlationId, "John Doe", "Vadodara", "john@doe.com", "+91-9988776655")
  agg.execute(NewCustomerApplication(Guid("")), cmd1, cmdCtx)

  print("System state after submitting the application -> ")
  //Finding the current state of an application
  val currentState1 = agg.apply(
    NewCustomerApplication(Guid("")),
    db.loadEventStream(correlationId).events as List<CustomerApplicationEvent>
  )
  println(currentState1)
  println()

  val cmd2 = ApproveCustomerApplication(correlationId, "Jane Doe")
  agg.execute(NewCustomerApplication(Guid("")), cmd2, cmdCtx)

  println("System state after approving the application -> ")
  //Finding the current state of an application
  val currentState = agg.apply(
    NewCustomerApplication(Guid("")),
    db.loadEventStream(correlationId).events as List<CustomerApplicationEvent>
  )
  println(currentState)
}
